@extends('layouts.app')

@section('content')
    <h1>Add New Todo</h1>
    {!! Form::open(['action' => ['TodosController@update',$todo->id], 'method'=>"POST"]) !!}
        {{ Form::bsText('Headline', $todo->text) }}
        {{ Form::bsTextArea('Todo', $todo->todo) }}
        {{ Form::bsText('DueDay', $todo->due) }}
        {{ Form::hidden('_method', 'PUT') }}
        {{ Form::bsSubmit('Submit',['class'=>'btn btn-primary']) }}
    {!! Form::close() !!}
@endsection