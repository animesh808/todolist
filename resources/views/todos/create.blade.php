@extends('layouts.app')

@section('content')
    <h1>Add New Todo</h1>
    {!! Form::open(['action' => 'TodosController@store', 'method'=>"POST"]) !!}
        {{ Form::bsText('Headline') }}
        {{ Form::bsTextArea('Todo') }}
        {{ Form::bsText('DueDay') }}
        {{ Form::bsSubmit('Submit',['class'=>'btn btn-primary']) }}
    {!! Form::close() !!}
@endsection