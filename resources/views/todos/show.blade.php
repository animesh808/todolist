@extends('../layouts.app')

@section('content')
<div class="well">
    <h2><a href="todo/{{$todo->id}}">{{$todo->text}}</a></h2>
    <label class="label label-danger">{{$todo->due}}</label>
    <hr>
    {{$todo->todo}}
</div>
<a href="/todo/{{$todo->id}}/edit" class="btn btn-primary">Edit</a>
{!! Form::open(['action' => ['TodosController@destroy',$todo->id], 'method'=>"POST",'class'=>'pull-right']) !!}
    {{ Form::hidden('_method', 'DELETE') }}
    {{ Form::bsSubmit('Delete',['class'=>'btn btn-danger']) }}
{!! Form::close() !!}
@endsection